package com.devsuperior.bds02.services;

import com.devsuperior.bds02.dto.CityDTO;
import com.devsuperior.bds02.entities.City;
import com.devsuperior.bds02.repositories.CityRepository;
import com.devsuperior.bds02.services.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CityService {

  private final CityRepository repository;

  @Autowired
  public CityService(CityRepository repository) {
	this.repository = repository;
  }

  @Transactional(readOnly = true)
  public Page<CityDTO> findAll(Pageable pageable) {
	return repository.findAll(pageable).map(CityDTO::new);
  }

  @Transactional
  public CityDTO insert(CityDTO cityDTO) {
	var city = new City(cityDTO.getId(), cityDTO.getName());
	city = repository.save(city);
	return new CityDTO(city);
  }

  public void delete(Long id) {
	try {
	  repository.deleteById(id);
	} catch(EmptyResultDataAccessException e) {
	  throw new ResourceNotFoundException("Id not found");
	} catch(DataIntegrityViolationException e) {
	  throw new DatabaseException("Integrative violation");
	}
  }

}
