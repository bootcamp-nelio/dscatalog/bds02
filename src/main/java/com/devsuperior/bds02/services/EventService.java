package com.devsuperior.bds02.services;

import com.devsuperior.bds02.dto.EventDTO;
import com.devsuperior.bds02.entities.City;
import com.devsuperior.bds02.repositories.EventRepository;
import com.devsuperior.bds02.services.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;

@Service
public class EventService {

  private final EventRepository repository;

  @Autowired
  public EventService(EventRepository repository) {
	this.repository = repository;
  }

  @Transactional
  public EventDTO update(Long id, EventDTO eventDTO) {
	try {
	  var event = repository.getOne(id);

	  event.setDate(eventDTO.getDate());
	  event.setName(eventDTO.getName());
	  event.setUrl(eventDTO.getUrl());
	  event.setCity(new City(eventDTO.getCityId(), null));
	  event = repository.save(event);
	  return new EventDTO(event);

	} catch(EntityNotFoundException e) {
	  throw new ResourceNotFoundException("Id not found " + id);
	}
  }

}
