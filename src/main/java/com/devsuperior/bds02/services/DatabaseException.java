package com.devsuperior.bds02.services;

public class DatabaseException extends RuntimeException {

  public DatabaseException(String message) {
	super(message);
  }

}
